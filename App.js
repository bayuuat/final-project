
import { StyleSheet, Text, View, Image, StatusBar, TouchableOpacity, TextInput, Button } from 'react-native';
import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Provider } from 'react-redux'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Login from './screens/SignIn'
import Register from './screens/Register'
import Home from './screens/Home'
import Detail from './screens/Detail'
import About from './screens/About'
import store from './src/redux/store';

const HomeStack = createStackNavigator()
const AboutStack = createStackNavigator()
const Drawer = createDrawerNavigator()

const HomeStackScreen = ({ navigation }) => (
  <HomeStack.Navigator screenOptions={{
    headerStyle: { backgroundColor: '#000119' },
    headerTintColor: '#fff'
  }}>
    <HomeStack.Screen name="Login" component={Login} options={{ headerShown: false }} />
    <HomeStack.Screen name="Register" component={Register} options={{ headerShown: false }} />
    <HomeStack.Screen name="Home" component={Home} options={{ headerLeft: () => (<Icon.Button name='menu' size={30} backgroundColor='#000119' onPress={() => navigation.openDrawer()} />) }} />
    <HomeStack.Screen name="Detail" component={Detail} options={{ headerShown: false }} />
  </HomeStack.Navigator>
)

const AboutStackScreen = ({ navigation }) => (
  <AboutStack.Navigator screenOptions={{
    headerStyle: { backgroundColor: '#000119' },
    headerTintColor: '#fff'
  }}>
    <AboutStack.Screen name="About" component={About} options={{ headerLeft: () => (<Icon.Button name='menu' size={30} backgroundColor='#000119' onPress={() => navigation.openDrawer()} />) }} />
  </AboutStack.Navigator>
)

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Drawer.Navigator initialRoute="Home">
          <Drawer.Screen name="Home" component={HomeStackScreen} />
          <Drawer.Screen name="About" component={AboutStackScreen} />
        </Drawer.Navigator>
      </NavigationContainer>
    </Provider>
  )
}

export default App

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000119',
  }
})