import { combineReducers } from "redux"

const regisState = {
    title: 'REGISTER',
    secureTextEntry: true
}

const registerReducer = (state = regisState, action) => {
    if (action.type === 'SET_PASSWORD') {
        return {
            ...state,
            secureTextEntry: !action.secureTextEntry
        }
    }
    return state
}

const loginState = {
    title: 'LOGIN',
    secureTextEntry: true
}

const loginReducer = (state = loginState, action) => {
    if (action.type === 'SET_PASSWORD') {
        return {
            ...state,
            secureTextEntry: !action.secureTextEntry
        }
    } else {
        return state
    }
}

const reducer = combineReducers({
    registerReducer,
    loginReducer
})

export default reducer