import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, StatusBar, TouchableOpacity, TextInput, FlatList, ImageBackground, ActivityIndicator } from 'react-native';
import { useSelector } from 'redux'
import { connect } from 'react-redux'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

class Home extends Component {


    constructor(props) {
        super(props)
        this.state = {
            isLoading: true,
            datasource: null,
            index: ''
        }
    }

    fetchData = async () => {
        let response = await fetch('https://travel-fcd16.firebaseio.com/.json');
        let responseJson = await response.json();
        this.setState({
            isLoading: false,
            datasource: responseJson.items
        });
    };

    componentDidMount() {
        this.fetchData()
    }

    renderItem = ({ item }) => {
        const { navigate } = this.props.navigation;
        return (
            <TouchableOpacity style={styles.item} onPress={() => {
                navigate('Detail', { item })
            }}>
                <Image source={{ uri: item.image }} style={styles.itemImage} />
                <View style={styles.background}>
                    <View style={styles.itemContent}>
                        <Text style={styles.itemText}>{item.cityName}</Text>
                        <Text style={styles.itemDetail}>{item.countryName}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    sendData = ({ item }) => {
        console.log
        this.setState({
            index: 22
        })
        console.log('Data yang dikirim:', this.state)
    }


    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#000119' }}>
                    <ActivityIndicator size='large' color='red' />
                </View>

            )
        } else {

            return (
                <View style={styles.full}>
                    <StatusBar />
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <Text style={styles.headerText}>Find your adventure</Text>
                        </View>
                        <View style={styles.body}>
                            <FlatList
                                horizontal={true}
                                data={this.state.datasource}
                                renderItem={this.renderItem}
                                keyExtractor={({ id }, index) => index.toString()}
                            />
                        </View>
                    </View>
                </View>
            )
        }
    }
}

function mapStateToProps(state) {
    return {
        data: state.datasource,
        load: state.isLoading
    }
}

export default connect(mapStateToProps)(Home)

const styles = StyleSheet.create({
    full: {
        flex: 1,
        backgroundColor: '#000119'
    },
    container: {
        flex: 1,
        paddingVertical: 30,
        paddingLeft: 30
    },
    header: {
        flex: 1.3,
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'space-between'

    },
    headerText: {
        color: 'white',
        fontSize: 36
    },
    body: {
        flex: 3,
    },
    category: {
        marginVertical: 30,
        height: 30,
        width: 100,
        marginRight: 10,
        backgroundColor: '#E3001A',
        borderRadius: 20,
        alignItems: 'center'
    },
    item: {
        marginRight: 20
    },
    itemImage: {
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        width: 215,
        height: '80%'
    },
    background: {
        height: 65,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        backgroundColor: 'white',
    },
    itemContent: {
        paddingHorizontal: 25,
        paddingTop: 3,
        justifyContent: 'center',
    },
    itemText: {
        fontSize: 22,
        fontWeight: 'bold',
    },
    itemDetail: {
        fontSize: 9
    }
})