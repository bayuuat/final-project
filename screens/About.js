import React from 'react';
import { StyleSheet, Text, View, Image, StatusBar, TouchableOpacity } from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';

export default function App() {
    return (
        <View style={styles.container}>
            <StatusBar style="auto" />
            <View style={styles.header} >
                <View style={styles.backgroundFoto}>
                    <Image source={require('../images/foto.jpeg')} style={styles.image} />
                </View>
                <Text style={{ color: 'white', fontSize: 24, fontWeight: 'bold', marginVertical: 20 }}>Bayu Aditya Triwibowo</Text>
            </View>
            <View style={styles.skill}>
                <View style={styles.skillitem}>
                    <FontAwesome5 name="instagram" size={36} color="#E3001A" />
                    <Text style={{ color: 'white' }}>@bayuuat</Text>
                </View>
                <View style={styles.skillitem}>
                    <FontAwesome5 name="facebook" size={36} color="#E3001A" />
                    <Text style={{ color: 'white' }}>Bayu Aditya</Text>
                </View>
                <View style={styles.skillitem}>
                    <FontAwesome5 name="gitlab" size={36} color="#E3001A" />
                    <Text style={{ color: 'white' }}>@bayuuat</Text>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#000119',
        alignItems: 'center'
    },
    header: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30
    },
    backgroundFoto: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 200,
        width: 200,
        backgroundColor: '#E3001A',
        borderRadius: 100
    },
    image: {
        height: 180,
        width: 180,
        borderRadius: 90
    },
    skill: {
        flex: 2,
        width: 160
    },
    skillitem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: 10
    }
});
