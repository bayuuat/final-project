import React from 'react';
import { StyleSheet, Text, View, Image, StatusBar, TouchableOpacity } from 'react-native';

export default function App() {
    return (
        <View style={styles.container}>
            <StatusBar style="auto" />
            <View >
                <View style={styles.logo}>
                    <Image source={require('../images/Logo.png')} />
                </View>
                <View style={styles.textLogo}>
                    <Image source={require('../images/style1.png')} />
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#000119',
        alignItems: 'center'
    },
    logo: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textLogo: {
        flex: 1,
        justifyContent: 'flex-end'
    }
});
