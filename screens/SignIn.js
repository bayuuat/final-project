import React from 'react';
import { StyleSheet, Text, View, Image, StatusBar, TouchableOpacity, TextInput, Button } from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import { MaterialCommunityIcons as Icon } from '@expo/vector-icons'

const SignIn = ({ navigation }) => {
    const loginReducer = useSelector((state) => state.loginReducer)

    const dispatch = useDispatch()

    const showPass = (secureTextEntry) => {
        dispatch({ type: 'SET_PASSWORD', secureTextEntry: secureTextEntry })
    }

    const Input = (props) => {
        return (
            <View style={styles.box}>
                <TextInput style={styles.textinput} placeholder={props.title} />
            </View>
        )
    }

    const Password = (props) => {
        return (
            <View style={styles.box}>
                <TextInput style={styles.textinput}
                    secureTextEntry={loginReducer.secureTextEntry}
                    placeholder={props.title} />
                <Icon name='eye' size={20} color='black' onPress={showPass} />
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <View style={styles.logo}>
                <Image source={require('../images/Logokecil.png')} />
            </View>
            <View style={styles.header}>
                <Input title='Email' />
                <Password title='Password' />
            </View>
            <View style={styles.button}>
                <TouchableOpacity style={styles.btn} onPress={() => navigation.navigate('Home')}>
                    <View >
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white' }}>LOGIN</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate('Register')} >
                    <Text style={{ marginTop: 15, fontSize: 12, color: '#FFD032' }}>CREATE NEW ACCOUNT?</Text>
                </TouchableOpacity>
            </View>
        </View >
    )
}

export default SignIn

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000119',
    },
    logo: {
        flex: 3,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    header: {
        flex: 3,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        width: '80%',
    },
    box: {
        paddingHorizontal: 5,
        width: '80%',
        flexDirection: 'row',
        borderBottomWidth: 2,
        backgroundColor: 'white',
        borderRadius: 8,
        alignItems: 'center',
        marginBottom: 30
    },
    textinput: {
        flex: 1,
        width: '100%',
        height: 35,
        paddingHorizontal: 5,
        marginBottom: 5
    },
    button: {
        flex: 4,
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    btn: {
        width: '80%',
        height: 40,
        backgroundColor: '#E3001A',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center'
    }
})