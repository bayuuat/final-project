import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, StatusBar, TouchableOpacity, ScrollView, ImageBackground } from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';

const Detail = ({ route, navigation: { goBack } }) => {
    return (
        <View style={{ backgroundColor: 'white', flex: 1 }}>
            <ImageBackground source={{ uri: route.params.item.imagepotrait }}
                style={styles.image}
                imageStyle={{ borderBottomLeftRadius: 30, borderBottomRightRadius: 30 }}>
                <Text style={styles.Tagline}>Discover {route.params.item.cityName}</Text>
                <Text style={styles.Placename}>Explore the beauty of {route.params.item.cityName}</Text>
                <TouchableOpacity style={styles.icon} onPress={() => goBack()} >
                    <FontAwesome5 name="angle-left" size={28} color="white" />
                </TouchableOpacity>
            </ImageBackground>
            <ScrollView>
                <Text style={styles.headerTitle}>About The Place</Text>
                <Text style={styles.caption}>{route.params.item.paraf1}</Text>
                <Text style={styles.caption}>{route.params.item.paraf2}</Text>
            </ScrollView>
        </View >
    )
}

export default Detail

const styles = StyleSheet.create({
    full: {
        flex: 1,
        backgroundColor: '#000119',
        alignItems: 'center',
        justifyContent: 'center'
    },
    image: {
        width: '100%',
        height: 330,
        justifyContent: 'flex-end'
    },
    Tagline: {
        color: 'white',
        fontSize: 16,
        fontWeight: 'bold',
        paddingHorizontal: 14,
        marginBottom: 5
    },
    Placename: {
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold',
        paddingHorizontal: 14,
        marginBottom: 30
    },
    icon: {
        position: 'absolute',
        left: 20,
        top: 20,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30
    },
    headerTitle: {
        padding: 14,
        fontSize: 20,
        fontWeight: 'bold',
    },
    caption: {
        paddingHorizontal: 14,
        fontSize: 14,
        opacity: 0.8,
        justifyContent: 'flex-start',
        textAlign: 'justify',
        lineHeight: 25,
    }

}) 