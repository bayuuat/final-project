import React from 'react';
import { StyleSheet, Text, View, Image, StatusBar, TouchableOpacity, TextInput, Button, ScrollView } from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import { MaterialCommunityIcons as Icon } from '@expo/vector-icons'

const SignIn = ({ navigation }) => {
    const registerReducer = useSelector((state) => state.registerReducer)
    const dispatch = useDispatch()

    const showPass = (secureTextEntry) => {
        dispatch({ type: 'SET_PASSWORD', secureTextEntry: secureTextEntry })
    }

    const Input = (props) => {
        return (
            <View style={styles.box}>
                <TextInput style={styles.textinput} placeholder={props.title} />
            </View>
        )
    }

    const Password = (props) => {
        return (

            <View style={styles.box}>
                <TextInput {...props}
                    style={styles.textinput}
                    secureTextEntry={registerReducer.secureTextEntry}
                    placeholder={props.title} />
                <Icon name='eye' size={20} color='black' onPress={showPass} />
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <View style={styles.logo}>
                <Image source={require('../images/Logokecil.png')} />
            </View>
            <View style={styles.inputBox}>
                <Input title='Name' />
                <Input title='Email' />
                <Password title='Password' />
            </View>
            <View style={styles.button}>
                <TouchableOpacity style={styles.btn} onPress={() => navigation.navigate('Login')} >
                    <View >
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white' }}>REGISTER</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default SignIn

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000119',
    },
    logo: {
        flex: 2,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    inputBox: {
        flex: 3,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    box: {
        paddingHorizontal: 5,
        width: '80%',
        flexDirection: 'row',
        borderBottomWidth: 2,
        backgroundColor: 'white',
        borderRadius: 8,
        alignItems: 'center',
        marginBottom: 30
    },
    textinput: {
        flex: 1,
        width: '100%',
        height: 35,
        paddingHorizontal: 5,
        marginBottom: 5
    },
    button: {
        flex: 2,
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    btn: {
        width: '80%',
        height: 40,
        backgroundColor: '#E3001A',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center'
    }
})